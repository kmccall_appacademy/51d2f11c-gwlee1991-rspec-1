
def add (num1, num2)
  return num1 + num2
end

def subtract (num1, num2)
  return num1 - num2
end

def sum (array)
  sum = 0
  idx = 0
  while idx < array.length
    sum = sum + array[idx]
    idx += 1
  end

  return sum
end

def multiply (num1, num2)
  return num1 * num2
end

def power (num1, num2)
  return num1 ** num2
end

def factorial (num)
  product = 1
  idx = 2
  while idx <= num
    product *= idx
    idx += 1
  end

  return product
end
