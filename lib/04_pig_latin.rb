
def is_vowel(char)
  vowels = ['a', 'e', 'i', 'o', 'u']
  return vowels.index(char) != nil
end

def word_in_piglatin(word)
  vowels = ['a', 'e', 'i', 'o', 'u']
  first_letter = word[0]

  if is_vowel(first_letter)
    return word + 'ay'
  else
    i = 0
    while i < word.length
      char = word[i]
      if is_vowel(char) && char == 'u' && word[i - 1] == 'q'
        return word[i+1..word.length-1] + word[0..i] + 'ay'
      elsif is_vowel(char)
        return word[i..word.length-1] + word[0..i-1] + 'ay'
      end

      i += 1
    end
  end

  return word + 'ay'
end

def match_cap (old_word, new_word)
  first_letter = old_word[0]

  if first_letter == old_word[0].upcase()
    return new_word[0].upcase() + new_word[1..new_word.length-1].downcase()
  else
    return new_word
  end
end

def translate (string)
  words = string.split
  new_words = []

  i = 0
  while i < words.length
    old_word = words[i]
    new_word = word_in_piglatin(old_word)
    new_words.push(match_cap(old_word, new_word))
    i += 1
  end


  return new_words.join(" ")
end
