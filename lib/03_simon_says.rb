# # Simon Says
#
# ## Topics
#
# * functions
# * strings
# * default parameter values
#
# ## Hints
#
# When you make the second `repeat` test pass, you might break the
# **first**

def echo (string)
  return string
end

def shout (string)
  return string.upcase()
end

def repeat (string, num = 2) #'hello', 2
  repeatedString = string
  idx = 1

  while idx < num
    repeatedString += ' ' + string
    idx += 1
  end

  return repeatedString
end

def start_of_word(string, num)
  start = ''
  idx = 0

  while idx < num
    start += string[idx]
    idx += 1
  end

  return start
end

def first_word (string)
  return string.split[0]
end

def titleize (string)
  words = string.split
  little_words = ['over', 'and', 'the']
  capitalized_word = []

  idx = 0
  while idx < words.length
    if (little_words.index(words[idx])) && idx == 0
      capitalized_word.push(words[idx].capitalize)
    elsif (little_words.index(words[idx])) && idx != 0
      capitalized_word.push(words[idx])
    else
      capitalized_word.push(words[idx].capitalize)
    end

    idx += 1
  end

    return capitalized_word.join(" ")
end
